#include <stdint.h>

#define APU_SND_CHN_P1 bit0
#define APU_SND_CHN_P2 bit1
#define APU_SND_CHN_T bit2
#define APU_SND_CHN_N bit3
#define APU_SND_CHN_D bit4

#define APU_LEN_BITS (bit7|bit6|bit5|bit4|bit3)

#define APU_LOWPASS_FILTER
#define APU_ADDITIVE_SYNTH

enum APUReg
{
	APU_SQ1_VOL, 	//$4000	Duty and volume for square wave 1
	APU_SQ1_SWEEP, 	//$4001	Sweep control register for square wave 1
	APU_SQ1_LO,		//$4002	Low byte of period for square wave 1
	APU_SQ1_HI, 	//$4003	High byte of period and length counter value for square wave 1
	APU_SQ2_VOL, 	//$4004	Duty and volume for square wave 2
	APU_SQ2_SWEEP, 	//$4005	Sweep control register for square wave 2
	APU_SQ2_LO,		//$4006	Low byte of period for square wave 2
	APU_SQ2_HI, 	//$4007	High byte of period and length counter value for square wave 2
	APU_TRI_LINEAR,	//$4008	Triangle wave linear counter
	APU_FOO1,		//$4009	Unused, but is eventually accessed in memory-clearing loops
	APU_TRI_LO, 	//$400A	Low byte of period for triangle wave
	APU_TRI_HI, 	//$400B	High byte of period and length counter value for triangle wave
	APU_NOISE_VOL, 	//$400C	Volume for noise generator
	APU_FOO2,		//$400D	Unused, but is eventually accessed in memory-clearing loops
	APU_NOISE_LO, 	//$400E	Period and waveform shape for noise generator
	APU_NOISE_HI, 	//$400F	Length counter value for noise generator
	APU_DMC_FREQ, 	//$4010	Play mode and frequency for DMC samples
	APU_DMC_RAW, 	//$4011	7-bit DAC
	APU_DMC_START, 	//$4012	Start of DMC waveform is at address $C000 + $40*$xx
	APU_DMC_LEN, 	//$4013	Length of DMC waveform is $10*$xx + 1 bytes (128*$xx + 8 samples)
	OAMDMA, 		//$4014	Writing $xx copies 256 bytes by reading from $xx00-$xxFF and writing to OAMDATA ($2004)
	APU_SND_CHN,	//$4015	Sound channels enable and status
	JOY1_PORT,		//$4016	Joystick 1 data (R) and joystick strobe (W)
	APU_FRAME 		//$4017	Joystick 2 data (R) and frame counter control (W)
};

struct apu_env_data
{
	uint8_t divider;
	uint8_t decay;
	uint8_t start;
};

typedef struct apu_data
{
	int clock;
	uint8_t mute;
	double frame_counter;
	
	uint8_t sweep_counter[2];
	
	struct apu_env_data sq_env[2];
	struct apu_env_data noise_env;
	
	uint8_t sq_len[2];
	uint8_t tri_len;
	uint8_t noise_len;
	
	double sq_time[2];
	double tri_time;
	double noise_time;
	double dmc_time;
	
	uint8_t dmc_activate;
	uint16_t dmc_len;
	uint16_t dmc_addr;
	uint8_t dmc_active;
	double dmc_level;
	
	int noise_rng;
	
	int ignore_snd_chn;
	
	uint8_t regs[0x18];
	uint8_t reg_w[0x18];
	
	#ifndef ADDITIVE_SYNTH
	double old_ampl[63];
	int fir_counter;
	#endif
	
	uint8_t (*dmc_read)(uint16_t addr, void *user_data);
	void *dmc_read_user_data;
	
	uint8_t irq_frame;
	uint8_t irq_dmc;
} apu_data;

#ifdef __cplusplus
extern "C" {
#endif

int apu_sample(apu_data *apu, uint16_t **sample, uint16_t *samples_end, int num_samples);
void apu_precalc();

#ifdef __cplusplus
};
#endif
