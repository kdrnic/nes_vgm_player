#include <allegro.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <stdint.h>
#include <math.h>

#include "readfile.h"
#include "wav.h"
#include "apu.h"

#define bit0  0x1u
#define bit1  0x2u
#define bit2  0x4u
#define bit3  0x8u
#define bit4  0x10u
#define bit5  0x20u
#define bit6  0x40u
#define bit7  0x80u
#define bit8  0x100u
#define bit9  0x200u
#define bit10 0x400u
#define bit11 0x800u
#define bit12 0x1000u
#define bit13 0x2000u
#define bit14 0x4000u
#define bit15 0x8000u

int log_writes = 0;

struct data_block
{
	uint16_t addr;
	int siz;
	uint8_t *ptr;
};

//Emulate cartridge ROM
struct data_block data_blocks[100];
int num_data_blocks = 0;

uint8_t data_read(uint16_t addr, void *user_data)
{
	for(int i = 0; i < num_data_blocks; i++){
		if(data_blocks[i].addr < addr
			&& addr - data_blocks[i].addr < data_blocks[i].siz){
				return data_blocks[i].ptr[addr - data_blocks[i].addr];
		}
	}
	return 0;
}

//Seems VGM doesn't do a APU_SND_CHN register set always
apu_data apu = {.ignore_snd_chn = 1, .dmc_read = data_read};

void apu_write(uint16_t apu_addr, uint8_t apu_val)
{
	switch(apu_addr - 0x4000u){
		case APU_SQ1_VOL: 	
		case APU_SQ1_SWEEP: 	
		case APU_SQ1_LO:		
		case APU_SQ1_HI: 	
		case APU_SQ2_VOL: 	
		case APU_SQ2_SWEEP: 	
		case APU_SQ2_LO:		
		case APU_SQ2_HI: 	
		case APU_TRI_LINEAR:	
		case APU_FOO1:		
		case APU_TRI_LO: 	
		case APU_TRI_HI: 	
		case APU_NOISE_VOL: 	
		case APU_FOO2:		
		case APU_NOISE_LO: 	
		case APU_NOISE_HI: 	
		case APU_DMC_FREQ: 	
		case APU_DMC_RAW: 	
		case APU_DMC_START: 	
		case APU_DMC_LEN: 		
		case APU_SND_CHN: 			
		case APU_FRAME:
			apu.reg_w[apu_addr - 0x4000u] = 1;
			apu.regs[apu_addr - 0x4000u] = apu_val;
			if(log_writes) printf("$%04X $%02X\n", apu_addr, apu_val);
			break;
		default:
			printf("Unknown APU address %04X\n", apu_addr);
			break;
	}
}

int vgm_play(uint8_t **vgm_ptr_ptr, void (*apu_write)(uint16_t, uint8_t))
{
	uint16_t apu_addr;
	uint8_t apu_val;
	int end = 0;
	int has_samples = 0;
	
	int data_siz;
	uint8_t data_typ;
	uint16_t data_addr;
	
	#define vgm_ptr (*vgm_ptr_ptr)
	
	do{
		switch(*vgm_ptr){
			case 0x61:
				//Wait n samples, n can range from 0 to 65535 (approx 1.49 seconds). Longer pauses than this are represented by multiple wait commands. 
				has_samples += *(vgm_ptr + 1);
				has_samples += ((int) *(vgm_ptr + 2)) << 8;
				vgm_ptr += 2;
				break;
				
			case 0x62:
				//wait 735 samples (60th of a second), a shortcut for 0x61 0xdf 0x02
				has_samples += 735;
				break;
				
			case 0x63:
				//wait 882 samples (50th of a second), a shortcut for 0x61 0x72 0x03
				has_samples += 882;
				break;
				
			case 0x66:
				//end of sound data 
				end = 1;
				vgm_ptr--;
				break;
				
			case 0x70 ... 0x7F:
				//0x7n = wait n+1 samples
				break;
				
			case 0xB4:
				//NES APU, write value dd to register aa
				/*
				Note: Registers 00-1F equal NES address 4000-401F,
				registers 20-3E equal NES address 4080-409E, register 3F equals NES address 4023,
				registers 40-7F equal NES address 4040-407F.
				*/
				switch((apu_addr = *(vgm_ptr + 1))){
					case 0x00 ... 0x1F:
					case 0x40 ... 0x7F:
						break;
					case 0x20 ... 0x3E:
						apu_addr += 0x60;
						break;
					case 0x3F:
						apu_addr = 0x23;
						break;
				}
				apu_addr |= 0x4000;
				apu_val = *(vgm_ptr + 2);
				if(apu_write) apu_write(apu_addr, apu_val);
				vgm_ptr += 2;
				break;
			
			case 0x67:
				//data block
				vgm_ptr++;//command
				vgm_ptr++;//0x66
				data_typ = *vgm_ptr;
				vgm_ptr++;//data_typ
				data_siz = *((int *)vgm_ptr);
				vgm_ptr += 4;//datasiz
				
				//printf(" %d\n", data_siz);
				
				if(data_typ < 0xC0 || data_typ > 0xDF){
					printf("Unsupported data type %02X\n", data_typ);
					exit(1);
				}
				
				data_addr = *(vgm_ptr);
				data_addr += ((int) *(vgm_ptr + 1)) << 8;
				vgm_ptr += 2; //data_addr
				
				data_blocks[num_data_blocks].addr = data_addr;
				data_blocks[num_data_blocks].siz = data_siz - 2;
				data_blocks[num_data_blocks].ptr = vgm_ptr;
				num_data_blocks++;
				
				vgm_ptr += data_siz - 2;//data
				vgm_ptr--;//go back so vgm_ptr++ at end of switch doesn't break things
				break;
			
			//Unimplemented, not found in VGMs I tried
			case 0x68:
				//PCM RAM write
			case 0x90 ... 0x95:
				//DAC Stream Control Write: see below 
			default:
				printf("Unsupported command $%02X\n", *vgm_ptr);
				exit(1);
				break;
		}
		vgm_ptr++;
	} while(!has_samples && !end);
	
	#undef vgm_ptr
	
	return has_samples;
}

//Convert num samples to human time
void samples_to_hmsm(int vgm_nsamples, char *str)
{
	int vgm_msec = ((vgm_nsamples * 10) / 441) % 1000;
	int vgm_sec = vgm_nsamples / 44100;
	int vgm_min = vgm_sec / 60;
	int vgm_hrs = vgm_min / 60; //lol
	vgm_min %= 60;
	vgm_sec %= 60;
	sprintf(str, "%02d:%02d:%02d:%03d", vgm_hrs, vgm_min, vgm_sec, vgm_msec);
}

int main(int argc, char **argv)
{
	int do_inflate = 0;	//GZIP file
	int i;
	int silent = 0;		//Do not play, only record to wav
	int record = 0;		//Save to WAV
	char *vgm_fn = 0;
	char *fn_wav = 0;
	
	//Find filename in arguments
	for(i = 1; i < argc; i++){
		char *s;
		if((s = strstr(argv[i], ".vgz")) && (strlen(s) == 4)){
			do_inflate = 1;
			vgm_fn = argv[i];
		}
		else if((s = strstr(argv[i], ".vgm")) && (strlen(s) == 4)){
			vgm_fn = argv[i];
		}
		else if((s = strstr(argv[i], ".wav")) && (strlen(s) == 4)){
			fn_wav = argv[i];
			record = 1;
		}
		else if(!strcmp("-s", argv[i])){
			silent = 1;
		}
		else if(!strcmp("-r", argv[i])){
			record = 1;
		}
		else if(!strncmp("-mute", argv[i], 5)){
			char *p = argv[i] + 5;
			while(*p){
				switch(*p++){
					case '1':
						apu.mute |= APU_SND_CHN_P1;
						break;
					case '2':
						apu.mute |= APU_SND_CHN_P2;
						break;
					case 'D': case 'd':
						apu.mute |= APU_SND_CHN_D;
						break;
					case 'N': case 'n':
						apu.mute |= APU_SND_CHN_N;
						break;
					case 'T': case 't':
						apu.mute |= APU_SND_CHN_T;
						break;
				}
			}
		}
		else if(!strcmp("-log", argv[i])){
			log_writes = 1;
		}
	}
	if(!vgm_fn){
		puts("no file passed");
		exit(1);
	}
	
	char *buf0;
	char *buf1;
	long len = read_file(vgm_fn, &buf0, 1);
	//A filename to later save the rendering
	char fn_wav_buf[1024];
	if(!fn_wav){
		fn_wav = fn_wav_buf;
		strcpy(fn_wav, get_filename(vgm_fn));
		strcat(fn_wav, ".wav");
	}
	
	//Decompress GZIP files (.vgz)
	if(do_inflate){
		z_stream strm = {0};
		int status;
		buf1 = malloc(0x1600000); //16mb
		
		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;
		strm.next_in = (__typeof(strm.next_in)) buf0;
		strm.avail_in = len;
		strm.next_out = (__typeof(strm.next_in)) buf1;
		strm.avail_out = 0x1600000;
		
		status = inflateInit2(&strm, 15 | 32);
		if(status < 0){
			puts(strm.msg);
			exit(status);
		}
		inflate(& strm, Z_SYNC_FLUSH);
		if(!strm.avail_out){
			puts("VGM too big");
			exit(1);
		}
		inflateEnd(&strm);
		buf1 = realloc(buf1, strm.total_out);
		len = strm.total_out;
		free(buf0);
	}
	else{
		buf1 = buf0;
	}
	
	#define vgm buf1
	
	//Read VGM header
	if(strncmp(vgm, "Vgm ", 4)){
		puts("Not VGM file");
		exit(1);
	}
	int vgm_ver = *((int *)(vgm + 0x8));
	int vgm_rate = *((int *)(vgm + 0x24));
	apu.clock = *((int *)(vgm + 0x84));
	int vgm_data_offset = 0x40;
	if(vgm_ver >= 0x150){
		vgm_data_offset = 0x34 + *((int *)(vgm + 0x34));
	}
	uint8_t *vgm_data = (uint8_t *) vgm + vgm_data_offset;
	printf(
		"%d bytes\n"
		"VGM version %x.%02x\n"
		"Rate %d Hz\n"
		"APU clock %d Hz\n",
		(int) len,
		vgm_ver >> 8,
		vgm_ver & 0xFF,
		vgm_rate,
		apu.clock
	);
	
	const int audio_buf_siz = 1024;
	int audio_buf_fake[audio_buf_siz]; //For silent play
	//Calculate total song size by performing a dummy play
	int has_samples = 0;
	int vgm_nsamples = 0;
	uint8_t *vgm_ptr = vgm_data;
	while((has_samples = vgm_play(&vgm_ptr, 0))){
		vgm_nsamples += has_samples;
	}
	//Reset num_data_blocks to not have them duplicated
	num_data_blocks = 0;
	//Pad samples to fit audio buffer size
	vgm_nsamples += audio_buf_siz + (vgm_nsamples % audio_buf_siz);
	char vgm_dur_str[256];
	samples_to_hmsm(vgm_nsamples, vgm_dur_str);
	printf("Duration %s (%d samples)\n", vgm_dur_str, vgm_nsamples);
	
	//Do synthesis precalculation
	apu_precalc();
	
	//Init Allegro
	allegro_init();
	set_color_depth(8);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	install_keyboard();
	install_timer();
	if(install_sound(DIGI_AUTODETECT, MIDI_NONE, 0)){
		puts("Failed to init sound");
		exit(1);
	}
	set_display_switch_mode(SWITCH_BACKGROUND);
	//PRETTY PLEASE give us decent volume
	set_volume(255, 255);
	set_hardware_volume(255, 255);
	set_volume_per_voice(0);
	
	//Set up a palette
	PALETTE pal = {};
	generate_332_palette(pal);
	set_palette(pal);
	
	//Set up audio stream, audio_buf_siz samples per buffer, 16 bits, mono, 44.1kHz, full volume, centred
	AUDIOSTREAM *audio_stream = 0;
	if(!silent && !(audio_stream = play_audio_stream(audio_buf_siz, 16, 0, 44100, 255, 128))){
		puts("Failed to get audio stream");
		exit(1);
	}
	
	//Create a SAMPLE large enough to contain the whole song
	SAMPLE *vgm_wav = create_sample(16, 0, 44100, vgm_nsamples);
	uint16_t *vgm_wav_ptr = vgm_wav->data;
	
	//A double buffer for screen blitting
	BITMAP *db = create_bitmap(screen->w, screen->h);
	
	//Play the song
	puts((!silent) ? "Playing song..." : "Recording song...");
	vgm_ptr = vgm_data;
	int end = 0;
	uint16_t *audio_buf = 0, *audio_buf_ptr, *audio_buf_end;
	has_samples = 0;
	int curr_samples = 0;
	
	//Magic spell to avoid hiccups with first buffer
	for(int i = 0; i < 2; i++){
		do{
			audio_buf = (!silent) ? get_audio_stream_buffer(audio_stream) : audio_buf_fake;
		} while(!audio_buf);
		if(!silent) free_audio_stream_buffer(audio_stream);
	}
	
	//Main loop
	while((!key[KEY_ESC]) && (!end)){
		//Wait for a stream buffer
		do{
			audio_buf = (!silent) ? get_audio_stream_buffer(audio_stream) : audio_buf_fake;
		} while(!audio_buf);
		
		audio_buf_ptr = audio_buf;
		audio_buf_end = audio_buf + audio_buf_siz;
		
		//Fill buffer with new samples
		do {
			has_samples -= apu_sample(&apu, &audio_buf_ptr, audio_buf_end, has_samples);
			if(!has_samples){
				has_samples += vgm_play(&vgm_ptr, apu_write);
				if(!has_samples){
					end = 1;
					has_samples++;
				}
			}
		} while(audio_buf_ptr != audio_buf_end);
		
		//Osciloscope
		for(int x = 0; x < screen->w && x < audio_buf_siz - 1; x++){
			vline(db, x, 0, screen->h - 1, makecol(0, 0, 0));
			vline(db, x, (((long) audio_buf[x]) * screen->h) / 0xFFFF, (((long) audio_buf[x + 1]) * screen->h) / 0xFFFF, makecol(0, 0xFF, 0));
		}
		
		//Show file, time
		char curr_str[256];
		samples_to_hmsm(curr_samples, curr_str);
		textout_ex(db, font, vgm_fn, 0, db->h - 24, makecol(0xFF, 0xFF, 0xFF), -1);
		textout_ex(db, font, curr_str, 0, db->h - 16, makecol(0xFF, 0xFF, 0xFF), -1);
		textout_ex(db, font, vgm_dur_str, 0, db->h - 8, makecol(0xFF, 0xFF, 0xFF), -1);
		
		//Blit double buffer to video
		blit(db, screen, 0, 0, 0, 0, screen->w, screen->h);
		
		//Record to wav
		memcpy(vgm_wav_ptr, audio_buf, audio_buf_siz * 2);
		vgm_wav_ptr += audio_buf_siz;
		
		//release audio buffer (puts it in queue to be played)
		if(!silent) free_audio_stream_buffer(audio_stream);
		
		//Increment duration counter
		curr_samples += audio_buf_siz;
	}
	
	//Finally write wav to disk
	int error;
	if(record && (error = save_wav(fn_wav, vgm_wav))){
		printf("Couldn't save file %s, err no %d\n", fn_wav, error);
	}
	
	allegro_exit();
	
	return 0;
}
END_OF_MAIN()
